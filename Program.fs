﻿module AdventOfCode.AdventOfCode
open AdventOfCode

[
    ("Day 1", First.part_one, First.part_two)
    ("Day 2", Second.part_one, Second.part_two)
    ("Day 3", Third.part_one, Third.part_two)
    ("Day 4", Fourth.part_one, Fourth.part_two)
    ("Day 5", Fifth.part_one, Fifth.part_two)
    ("Day 6", Sixth.part_one, Sixth.part_two)
]
|> List.iter (fun (day, one, two) -> printfn "%s" day; one() |> printfn "part 1: %s" ; two() |> printfn "part 2: %s" ; printfn "")